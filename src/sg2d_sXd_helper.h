/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SG2D_S2D_HELPER_H__
#define SG2D_S2D_HELPER_H__

#include "sg2d.h"
#include <star/senc2d.h>

#include <rsys/rsys.h>

#include <limits.h>

/* Get vertex indices for the iseg_th segment.
 * Suitable for use as get_indice callback in s2d_mesh_setup_indexed_vertices
 * calls. */
static FINLINE void
sg2d_sXd_geometry_get_indices
  (const unsigned iseg,
   unsigned indices[SG2D_GEOMETRY_DIMENSION],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  int i;
  size_t tmp[SG2D_GEOMETRY_DIMENSION];
  ASSERT(indices && geometry);
  SG2D(geometry_get_unique_segment_vertices(geometry, iseg, tmp));
  FOR_EACH(i, 0, SG2D_GEOMETRY_DIMENSION) {
    ASSERT(tmp[i] <= UINT_MAX);
    indices[i] = (unsigned)tmp[i];
  }
}

/* Get coordinates for the ivert_th vertex.
 * Suitable for use as s2d_vertex_data getter for S2D_POSITION s2d_attrib_usage
 * in s2d_mesh_setup_indexed_vertices calls. */
static FINLINE void
sg2d_sXd_geometry_get_position
  (const unsigned ivert,
   float coord[SG2D_GEOMETRY_DIMENSION],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  double tmp[SG2D_GEOMETRY_DIMENSION];
  int i;
  ASSERT(coord && geometry);
  SG2D(geometry_get_unique_vertex(geometry, ivert, tmp));
  FOR_EACH(i, 0, SG2D_GEOMETRY_DIMENSION) coord[i] = (float)tmp[i];
}

#endif /* SG2D_S2D_HELPER_H__ */
