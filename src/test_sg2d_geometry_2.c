/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sg2d.h"
#include "test_sg2d_utils.h"

#include <stdio.h>

 /* Manage add_geometry behaviour */
struct add_geom_ctx {
  unsigned add_cpt, merge_cpt;
  res_T add_res, merge_res;
};

static res_T
add_seg
  (const unsigned unique_id,
   const unsigned iseg,
   void* context)
{
  struct context* ctx = context;
  struct add_geom_ctx* add_geom_ctx;
  ASSERT(ctx); (void)unique_id; (void)iseg;
  add_geom_ctx = ctx->custom;
  if(add_geom_ctx->add_res == RES_OK) ++add_geom_ctx->add_cpt;
  return add_geom_ctx->add_res;
}

static res_T
merge_seg
  (const unsigned unique_id,
   const unsigned iseg,
   const int reversed_segment,
   unsigned segment_properties[SG2D_PROP_TYPES_COUNT__],
   const unsigned merged_properties[SG2D_PROP_TYPES_COUNT__],
   void* context,
   int* merge_conflict)
{
  struct context* ctx = context;
  struct add_geom_ctx* add_geom_ctx;
  int i;
  ASSERT(ctx && segment_properties && merged_properties && merge_conflict);
  (void)unique_id; (void)iseg; (void)reversed_segment;
  (void)segment_properties; (void)merged_properties;
  add_geom_ctx = ctx->custom;
  if(add_geom_ctx->merge_res == RES_OK) ++add_geom_ctx->merge_cpt;
  FOR_EACH(i, 0, SG2D_PROP_TYPES_COUNT__)
    if(!sg2d_compatible_property(segment_properties[i], merged_properties[i]))
      *merge_conflict = 1;
  return add_geom_ctx->merge_res;
}

static res_T
validate
  (const unsigned iseg,
   const unsigned properties[SG2D_PROP_TYPES_COUNT__],
   void* context,
   int* properties_conflict)
{
  (void)iseg; (void)properties; (void)context;
  *properties_conflict = 0;
  return RES_OK;
}

static res_T
validate2
  (const unsigned iseg,
   const unsigned properties[SG2D_PROP_TYPES_COUNT__],
   void* context,
   int* properties_conflict)
{
  (void)iseg; (void)properties; (void)context;
  *properties_conflict = (iseg % 2 == 0) ? 0 : (int)iseg;
  return RES_OK;
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sg2d_device* dev;
  struct sg2d_geometry* geom;
  struct context ctx = CONTEXT_NULL__;
  struct sg2d_geometry_add_callbacks callbacks = SG2D_ADD_CALLBACKS_NULL__;
  struct add_geom_ctx add_geom_ctx;
  unsigned property[12];
  unsigned i;
  const unsigned property_count = sizeof(property) / sizeof(*property);
  unsigned count;
  (void)argc, (void)argv;
  
  OK(mem_init_proxy_allocator(&allocator, &mem_default_allocator));
  OK(sg2d_device_create(NULL, &allocator, 1, &dev));
  OK(sg2d_geometry_create(dev, &geom));

  /* A 2D square.
   * 2 enclosures (inside, outside) sharing the same segments,
   * but opposite sides */
  ctx.positions = square_vertices;
  ctx.indices = box_indices;
  ctx.custom = &add_geom_ctx;

  add_geom_ctx.add_cpt = add_geom_ctx.merge_cpt = 0;
  add_geom_ctx.add_res = add_geom_ctx.merge_res = RES_OK;

  /* Geometry with no media information on both sides */
  for(i = 0; i < property_count; i++) property[i] = SG2D_UNSPECIFIED_PROPERTY;
  ctx.front_media = property;
  ctx.back_media = property;
  ctx.intface = property;

  callbacks.get_indices = get_indices;
  callbacks.get_properties = get_properties;
  callbacks.get_position = get_position;
  callbacks.add_segment = add_seg;
  callbacks.merge_segment = merge_seg;

  /* If add fails, add geometry fails the same way */
  add_geom_ctx.add_res = RES_BAD_ARG;

  BA(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.add_cpt == 0);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == 0);
  add_geom_ctx.add_res = RES_MEM_ERR;
  ME(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.add_cpt == 0);
  CHK(count == 0);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 2 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Successful add geometry with add callback */
  add_geom_ctx.add_res = RES_OK;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.add_cpt == nsegments);
  CHK(add_geom_ctx.merge_cpt == 0);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 3 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "test_unspecified",
    SG2D_C_DUMP_STATIC | SG2D_C_DUMP_CONST));

  /* Clear geometry */
  SG2D(geometry_ref_put(geom));
  OK(sg2d_geometry_create(dev, &geom));

  /* Successful add geometry without add callback */
  add_geom_ctx.add_cpt = 0;
  callbacks.add_segment = NULL;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.add_cpt == 0);
  CHK(add_geom_ctx.merge_cpt == 0);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* If merge fails, add geometry fails the same way */
  add_geom_ctx.merge_res = RES_BAD_ARG;
  callbacks.add_segment = add_seg;
  BA(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == 0);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 2 * nsegments);
  add_geom_ctx.merge_res = RES_MEM_ERR;
  ME(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == 0);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 3 * nsegments);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0); /* merge failed but with a no-conflict status */
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Successful add geometry without merge callback */
  callbacks.merge_segment = NULL;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == 0);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 4 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Successful add geometry with merge callback */
  add_geom_ctx.merge_res = RES_OK;
  callbacks.merge_segment = merge_seg;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == nsegments);
  add_geom_ctx.merge_cpt = 0;
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 5 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0); /* merge failed but with a no-conflict status */
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Geometry with media information on both sides */
  ctx.front_media = medium0;
  ctx.back_media = medium1;

  /* Clear geometry */
  SG2D(geometry_ref_put(geom));
  OK(sg2d_geometry_create(dev, &geom));

  /* Successful add geometry with add callback
   * First half of the segments, then all of them */
  add_geom_ctx.add_res = RES_OK;
  OK(sg2d_geometry_add(geom, nvertices, nsegments / 2, &callbacks, &ctx));
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments / 2);
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.add_cpt == nsegments);
  CHK(add_geom_ctx.merge_cpt == nsegments / 2);
  add_geom_ctx.add_cpt = 0;
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments + nsegments / 2);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0); /* media where defined */
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments); /* interfaces where unspecified */
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_dump_as_vtk(geom, stdout));
  /* Second add was half duplicated, so numbering is shifted */
  FOR_EACH(i, 0, nsegments) {
    unsigned id;
    OK(sg2d_geometry_get_unique_segment_user_id(geom, i, &id));
    CHK(i < nsegments / 2 ? id == i : id == i + nsegments / 2);
  }

  /* Clear geometry */
  SG2D(geometry_ref_put(geom));
  OK(sg2d_geometry_create(dev, &geom));
  add_geom_ctx.merge_cpt = 0;

  /* Successful add geometry with add callback and no defined properties */
  add_geom_ctx.add_res = RES_OK;
  callbacks.get_properties = NULL;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.add_cpt == nsegments);
  CHK(add_geom_ctx.merge_cpt == 0);
  add_geom_ctx.add_cpt = 0;
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments); /* media where unspecified */
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == nsegments); /* interfaces where unspecified */
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Define interface */
  ctx.intface = intface0;

  /* Successful add geometry with merge callback and properties */
  add_geom_ctx.merge_res = RES_OK;
  callbacks.get_properties = get_properties;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == nsegments);
  add_geom_ctx.merge_cpt = 0;
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 2 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0); /* media where defined */
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == 0); /* interfaces where defined */
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Geometry with incompatible media information on both sides */
  ctx.front_media = medium1;
  ctx.back_media = medium0;

  /* Add geometry without merge callback and conflicts */
  callbacks.merge_segment = NULL;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == 0);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 3 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0); /* media where defined */
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == 0); /* interfaces where defined */
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Incompatible interface */
  ctx.intface = intface1;

  /* Successful add geometry with merge callback */
  add_geom_ctx.merge_res = RES_OK;
  callbacks.merge_segment = merge_seg;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  CHK(add_geom_ctx.merge_cpt == nsegments);
  add_geom_ctx.merge_cpt = 0;
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 4 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0); /* media where defined */
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == 0); /* interfaces where defined */
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  /* Clear geometry */
  SG2D(geometry_ref_put(geom));
  OK(sg2d_geometry_create(dev, &geom));

  /* Successful add geometry with merge callback */
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);

  OK(sg2d_geometry_validate_properties(geom, validate, NULL));
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_dump_as_obj(geom, stdout, SG2D_OBJ_DUMP_ALL));

  OK(sg2d_geometry_validate_properties(geom, validate, NULL));
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);

  OK(sg2d_geometry_validate_properties(geom, validate2, NULL));
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == nsegments / 2);

  OK(sg2d_geometry_ref_put(geom));
  OK(sg2d_device_ref_put(dev));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
