/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sg2.h"
#include "test_sg2d_utils.h"

#include <stdio.h>

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sg2d_device* dev;
  struct sg2d_geometry* geom;
  struct context ctx = CONTEXT_NULL__;
  struct sg2d_geometry_add_callbacks callbacks = SG2D_ADD_CALLBACKS_NULL__;
  unsigned property[12];
  unsigned i;
  const unsigned property_count = sizeof(property) / sizeof(*property);
  unsigned count;
  (void)argc, (void)argv;
  
  OK(mem_init_proxy_allocator(&allocator, &mem_default_allocator));
  OK(sg2d_device_create(NULL, &allocator, 1, &dev));
  OK(sg2d_geometry_create(dev, &geom));

  FOR_EACH(i, 0, property_count) property[i] = SG2D_UNSPECIFIED_PROPERTY;

  callbacks.get_indices = get_indices;
  callbacks.get_position = get_position;

  /* A 3D square.
   * 2 enclosures (inside, outside) sharing the same segments,
   * but opposite sides */
  ctx.positions = box_vertices;
  ctx.indices = box_indices;

  /* Add geometry with no properties */
  ctx.front_media = property;
  ctx.back_media = medium1;
  ctx.intface = property;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments);

  /* Add same geometry with no properties on front/intface */
  callbacks.get_properties = get_properties;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 2 * nsegments);

  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "front_unspecified",
    SG2D_C_DUMP_STATIC | SG2D_C_DUMP_CONST));

  /* Add same geometry, front/intface properties are defined for odd segments */
  FOR_EACH(i, 0, sizeof(property) / sizeof(*property))
    property[i] = (i % 2) ? 0 : SG2D_UNSPECIFIED_PROPERTY;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments / 2);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 3 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  FOR_EACH(i, 0, count) {
    unsigned prop[SG2D_PROP_TYPES_COUNT__];
    OK(sg2d_geometry_get_unique_segment_properties(geom, i, prop));
    CHK(prop[SG2D_FRONT] == ((i % 2) ? 0 : SG2D_UNSPECIFIED_PROPERTY)
      && prop[SG2D_BACK] == 1
      && prop[SG2D_INTFACE] == ((i % 2) ? 0 : SG2D_UNSPECIFIED_PROPERTY));
  }

  /* Same information again, using a reversed box */
  ctx.reverse_vrtx = 1;
  SWAP(const unsigned*, ctx.front_media, ctx.back_media);
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == nsegments / 2);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 4 * nsegments);

  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "front_half_unspecified",
    SG2D_C_DUMP_STATIC | SG2D_C_DUMP_CONST));

  /* Define properties for remaining segments, using reversed box */
  FOR_EACH(i, 0, sizeof(property) / sizeof(*property))
    property[i] = (i % 2) ? SG2D_UNSPECIFIED_PROPERTY : 0;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));
  CHK(count == nvertices);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 5 * nsegments);
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));
  CHK(count == nsegments);
  FOR_EACH(i, 0, count) {
    unsigned prop[SG2D_PROP_TYPES_COUNT__];
    OK(sg2d_geometry_get_unique_segment_properties(geom, i, prop));
    CHK(prop[SG2D_FRONT] == 0 && prop[SG2D_BACK] == 1
      && prop[SG2D_INTFACE] == 0);
  }

  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "all_defined",
    SG2D_C_DUMP_STATIC | SG2D_C_DUMP_CONST));

  /* Define incoherent properties for some segments */
  FOR_EACH(i, 0, sizeof(property) / sizeof(*property))
    property[i] = (i % 2);
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == nsegments / 2);
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 6 * nsegments);

  OK(sg2d_geometry_ref_put(geom));
  OK(sg2d_device_ref_put(dev));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
