/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sg2.h"
#include "test_sg2d_utils.h"
#include "test_sg2d_utils2.h"

#include <rsys/double2.h>

#include <stdio.h>
#include <limits.h>

#define NB_CIRC 4

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sg2d_device* dev;
  struct sg2d_geometry* geom;
  struct sg2d_geometry_add_callbacks callbacks = SG2D_ADD_CALLBACKS_NULL__;
  unsigned circ_seg_count, circ_vrtx_count, i, count;
  unsigned m0 = 0, m1, itf = 0;
  struct context ctx = CONTEXT_NULL__;
  (void)argc, (void)argv;

  OK(mem_init_proxy_allocator(&allocator, &mem_default_allocator));
  OK(sg2d_device_create(NULL, &allocator, 1, &dev));
  OK(sg2d_geometry_create(dev, &geom));
  SG2(device_ref_put(dev));

  callbacks.get_indices = get_indices;
  callbacks.get_position = get_position;
  callbacks.get_properties = get_uniform_properties;

  ctx.front_media = &m1;
  ctx.back_media = &m0;
  ctx.intface = &itf;

  /* A 264 segments circle template */
  create_circle(1, 264, &ctx);
  ASSERT(sa_size(ctx.positions) % 2 == 0
    && sa_size(ctx.positions) / 2 < UINT_MAX);
  ASSERT(sa_size(ctx.indices) % 2 == 0
    && sa_size(ctx.indices) / 2 < UINT_MAX);
  circ_seg_count = (unsigned)sa_size(ctx.indices) / 2;
  circ_vrtx_count = (unsigned)sa_size(ctx.positions) / 2;

  OK(sg2d_geometry_reserve(geom, NB_CIRC * circ_vrtx_count,
    NB_CIRC * circ_seg_count, 0));
  FOR_EACH(i, 0, NB_CIRC) {
    m1 = i;
    d2(ctx.offset, 0, i * 10);
    OK(sg2d_geometry_add(geom, circ_vrtx_count, circ_seg_count, &callbacks,
      &ctx));
  }
  circle_release(&ctx);

  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0);

  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "some_segments",
    SG2D_C_DUMP_CONST | SG2D_C_DUMP_STATIC));

  SG2(geometry_ref_put(geom));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}