/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sg2d.h"
#include "test_sg2d_utils.h"

#include <rsys/double3.h>

#include <stdio.h>

static const double
square_vertices2[4/*#vertices*/ * 2/*#coords per vertex*/] = {
  0.0, 0.0,
  2.0, 0.0,
  0.0, 1.0,
  2.0, 1.0
};

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sg2d_device* dev;
  struct sg2d_geometry* geom;
  struct context ctx = CONTEXT_NULL__;
  struct sg2d_geometry_add_callbacks callbacks = SG2D_ADD_CALLBACKS_NULL__;
  unsigned count;
  (void)argc, (void)argv;

  OK(mem_init_proxy_allocator(&allocator, &mem_default_allocator));
  OK(sg2d_device_create(NULL, &allocator, 1, &dev));
  OK(sg2d_geometry_create(dev, &geom));
  SG2D(device_ref_put(dev));

  callbacks.get_indices = get_indices;
  callbacks.get_properties = get_properties;
  callbacks.get_position = get_position;

  ctx.positions = square_vertices;
  ctx.indices = box_indices;
  ctx.front_media = medium0;
  ctx.back_media = medium1;
  ctx.intface = intface0;

  /* First square (front: 0, back: 1), right-handed normal outside */
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));

  ctx.positions = square_vertices2;

  /* Second square (front: 0, back: 1), right-handed normal outside 
   * Some segments are indentical and others longer than first square segments */
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));

  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));
  CHK(count == 0);
  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "invalid",
    SG2D_C_DUMP_CONST | SG2D_C_DUMP_STATIC));

  SG2D(geometry_ref_put(geom));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}