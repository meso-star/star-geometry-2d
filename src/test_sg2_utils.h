/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_SG2D_UTILS_H
#define TEST_SG2D_UTILS_H

#include <rsys/mem_allocator.h>
#include <stdio.h>

#define OK(Cond) CHK((Cond) == RES_OK)
#define BA(Cond) CHK((Cond) == RES_BAD_ARG)
#define ME(Cond) CHK((Cond) == RES_MEM_ERR)

/*******************************************************************************
 * Memory allocator
 ******************************************************************************/
static INLINE void
check_memory_allocator(struct mem_allocator* allocator)
{
  if(MEM_ALLOCATED_SIZE(allocator)) {
    char dump[80192];
    MEM_DUMP(allocator, dump, sizeof(dump));
    fprintf(stderr, "%s\n", dump);
    FATAL("Memory leaks.\n");
  }
}

/*******************************************************************************
 * Geometry
 ******************************************************************************/
/* 2D square */
static const double
square_vertices[4/*#vertices*/ * 2/*#coords per vertex*/] = {
  0.0, 0.0,
  1.0, 0.0,
  0.0, 1.0,
  1.0, 1.0
};
static const unsigned
nvertices = sizeof(square_vertices) / (2 * sizeof(*square_vertices));
/* Distorded square */
static const double
box_vertices[4/*#vertices*/ * 2/*#coords per vertex*/] = {
  0.1, 0.0,
  1.0, 0.0,
  0.0, 1.1,
  1.0, 1.0
};

/* The following array lists the indices toward the 2D vertices of each
 * segment.
 *                   Y
 *    2----3         |
 *    |    |         0----X
 *    |    |
 *    0----1
 *
 * The right-handed geometrical normal is outside the square
 */
static unsigned
box_indices[4/*#segments*/ * 2/*#indices per segment*/] = {
  0, 2,
  2, 3,
  3, 1,
  1, 0
};
static const unsigned
nsegments = sizeof(box_indices) / (2 * sizeof(*box_indices));

struct context {
  const double* positions;
  const unsigned* indices;
  const unsigned* front_media;
  const unsigned* back_media;
  const unsigned* intface;
  void* custom;
  double offset[2];
  double scale;
  char reverse_vrtx, reverse_med;
};
#define CONTEXT_NULL__ {\
  NULL, NULL, NULL, NULL, NULL, NULL, {0,0}, 1, 0, 0\
}

static const unsigned medium0[4] = { 0, 0, 0, 0 };
static const unsigned medium1[4] = { 1, 1, 1, 1 };
static const unsigned medium2[4] = { 2, 2, 2, 2 };
static const unsigned medium1_3[4] = { 1, 1, 3, 1 };
static const unsigned medium1_bottom0[4] = { 1, 1, 1, 0 };
static const unsigned medium1_top0[4] = { 1, 0, 1, 1 };

static const unsigned intface0[4] = { 0, 0, 0, 0 };
static const unsigned intface1[4] = { 1, 1, 1, 1 };

static INLINE void
get_indices(const unsigned iseg, unsigned ids[2], void* context)
{
  const struct context* ctx = context;
  ASSERT(ids && ctx);
  ids[ctx->reverse_vrtx ? 1 : 0] = ctx->indices[iseg * 2 + 0];
  ids[ctx->reverse_vrtx ? 0 : 1] = ctx->indices[iseg * 2 + 1];
}

static INLINE void
get_position(const unsigned ivert, double pos[2], void* context)
{
  const struct context* ctx = context;
  ASSERT(pos && ctx);
  pos[0] = ctx->positions[ivert * 2 + 0] * ctx->scale + ctx->offset[0];
  pos[1] = ctx->positions[ivert * 2 + 1] * ctx->scale + ctx->offset[1];
}

static INLINE void
get_properties
  (const unsigned iseg,
   unsigned property[SG2D_PROP_TYPES_COUNT__],
   void* context)
{
  const struct context* ctx = context;
  ASSERT(property && ctx);
  property[ctx->reverse_med ? SG2D_BACK : SG2D_FRONT] = ctx->front_media[iseg];
  property[ctx->reverse_med ? SG2D_FRONT : SG2D_BACK] = ctx->back_media[iseg];
  property[SG2D_INTFACE] = ctx->intface[iseg];
}

#endif /* TEST_SG2D_UTILS_H */
