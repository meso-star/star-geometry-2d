/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sg2d.h"
#include "sg2d_geometry.h"
#include "sg2d_device.h"

#include <rsys/double2.h>

#include <limits.h>

/******************************************************************************
 * Helper functions
 *****************************************************************************/
static void
geometry_release(ref_T* ref)
{
  struct sg2d_geometry* geom;
  struct sg2d_device* dev;
  ASSERT(ref);
  geom = CONTAINER_OF(ref, struct sg2d_geometry, ref);
  dev = geom->dev;
  darray_segment_release(&geom->unique_segments);
  darray_vertex_release(&geom->unique_vertices);
  htable_seg_release(&geom->unique_segments_ids);
  htable_vrtx_release(&geom->unique_vertices_ids);
  darray_seg_descriptions_release(&geom->seg_descriptions);
  MEM_RM(dev->allocator, geom);
  SG2D(device_ref_put(dev));
}

static FINLINE int /* Return 1 if reversed */
seg_make_key(struct vrtx_id2* k, const vrtx_id_t t[2])
{
  ASSERT(t);
  ASSERT(t[0] != t[1]);
  if(t[0] < t[1]) {
    k->x[0] = t[0];
    k->x[1] = t[1];
    return 0;
  } else {
    k->x[0] = t[1];
    k->x[1] = t[0];
    return 1;
  }
}

static void
dump_seg_property
  (const struct sg2d_geometry* geom,
   FILE* stream,
   const enum sg2d_property_type type)
{
  size_t i;
  const struct seg_descriptions* descriptions;
  ASSERT(geom && stream && type < SG2D_PROP_TYPES_COUNT__);

  descriptions
    = darray_seg_descriptions_cdata_get(&geom->seg_descriptions);
  FOR_EACH(i, 0, darray_segment_size_get(&geom->unique_segments)) {
    prop_id_t property = SG2D_UNSPECIFIED_PROPERTY;
    size_t tdefs_count
      = darray_definition_size_get(&descriptions[i].defs[type]);
    if(tdefs_count && descriptions[i].property_defined[type]) {
      const struct definition* tdefs
        = darray_definition_cdata_get(&descriptions[i].defs[type]);
      size_t j;
      FOR_EACH(j, 0, tdefs_count) {
        if(tdefs->property_value != SG2D_UNSPECIFIED_PROPERTY) {
          property = tdefs->property_value;
          break; /* Found the defined value */
        }
        tdefs++; /* Next value */
      }
    }
    /* In VTK dumps UINT_MAX is used for unspecified */
    if(property == SG2D_UNSPECIFIED_PROPERTY)
      fprintf(stream, "%u\n", UINT_MAX);
    else fprintf(stream, "%u\n", (unsigned)property);
  }
}

/******************************************************************************
 * Local functions
 *****************************************************************************/
res_T
geometry_register_segment
  (struct sg2d_geometry* geom,
   const struct segment* segment,
   const seg_id_t segment_unique_id,
   const unsigned set_id,
   const int merge_conflict)
{
  res_T res = RES_OK;
  struct seg_descriptions* seg_d;
  int i;
  char keep_prop_def[SG2D_PROP_TYPES_COUNT__];

  ASSERT(geom && segment);

  ERR(geometry_enlarge_seg_descriptions(geom, segment_unique_id + 1));
  seg_d = (darray_seg_descriptions_data_get(&geom->seg_descriptions)
    + segment_unique_id);
  /* Record information */
  FOR_EACH(i, 0, SG2D_PROP_TYPES_COUNT__) {
    struct darray_definition* definitions;
    struct definition* defs;
    int done = 0;
    size_t j;
    keep_prop_def[i] = seg_d->property_defined[i];
    if(segment->properties[i] == SG2D_UNSPECIFIED_PROPERTY)
      seg_d->defs_include_unspecified = 1;
    else seg_d->property_defined[i] = 1;
    definitions = seg_d->defs + i;
    defs = darray_definition_data_get(definitions);
    FOR_EACH(j, 0, darray_definition_size_get(definitions)) {
      if(defs[j].property_value == segment->properties[i]) {
        /* This property_value is already registered: no conflict */
        const unsigned* ids = darray_uint_cdata_get(&defs[j].set_ids);
        size_t k;
        /* Search if property_value already includes set_id */
        FOR_EACH(k, 0, darray_uint_size_get(&defs[j].set_ids)) {
          if(ids[k] == set_id) {
            /* Same value+set_id was there already */
            done = 1;
            break;
          }
        }
        if(!done) {
          /* Need to add the set_id for this property_value */
          ERR(darray_uint_push_back(&defs[j].set_ids, &set_id));
          done = 1;
        }
        break;
      }
    }
    if(!done) {
      /* This property_value was not recorded already */
      size_t defs_sz = darray_definition_size_get(definitions);
      struct definition* new_def;
      ERR(darray_definition_resize(definitions, 1 + defs_sz));
      new_def = darray_definition_data_get(definitions) + defs_sz;
      ERR(darray_uint_push_back(&new_def->set_ids, &set_id));
      new_def->property_value = segment->properties[i];
      if(!seg_d->merge_conflict && merge_conflict) {
        /* If more than 1 merge_conflict occur, the first one remains */
        seg_d->merge_conflict = merge_conflict;
        geom->merge_conflict_count++;
      }
    }
  }

  if((!keep_prop_def[SG2D_FRONT] || !keep_prop_def[SG2D_BACK])
    && seg_d->property_defined[SG2D_FRONT] && seg_d->property_defined[SG2D_BACK])
  {
    /* Both sides are now defined */
    ASSERT(geom->seg_with_unspecified_sides_count > 0);
    geom->seg_with_unspecified_sides_count--;
  }

  if(!keep_prop_def[SG2D_INTFACE] && seg_d->property_defined[SG2D_INTFACE]) {
    /* Interface is now defined */
    ASSERT(geom->seg_with_unspecified_intface_count > 0);
    geom->seg_with_unspecified_intface_count--;
  }

exit:
  return res;
error:
  goto exit;
}

res_T
geometry_enlarge_seg_descriptions
  (struct sg2d_geometry* geom,
   const seg_id_t sz)
{
  res_T res = RES_OK;
  size_t old_sz =
    darray_seg_descriptions_size_get(&geom->seg_descriptions);
  if(sz <= old_sz) return RES_OK;
  ERR(darray_seg_descriptions_resize(&geom->seg_descriptions, sz));
  ASSERT(geom->seg_with_unspecified_sides_count + sz - old_sz <= SEG_MAX__);
  ASSERT(geom->seg_with_unspecified_intface_count + sz - old_sz <= SEG_MAX__);
  geom->seg_with_unspecified_sides_count += (seg_id_t)(sz - old_sz);
  geom->seg_with_unspecified_intface_count += (seg_id_t)(sz - old_sz);

exit:
  return res;
error:
  goto exit;
}

static void
dump_partition
  (const struct sg2d_geometry* geom,
   FILE* stream,
   const char* group_name,
   enum sg2d_obj_dump_content partition)
{
  const struct seg_descriptions* seg_descriptions;
  const struct segment* segments;
  size_t sz, i;
  ASSERT(geom && stream && group_name);
  ASSERT(partition == SG2D_OBJ_DUMP_MERGE_CONFLICTS
    || partition == SG2D_OBJ_DUMP_PROPERTY_CONFLICTS
    || partition == SG2D_OBJ_DUMP_VALID_PRIMITIVE);
  seg_descriptions
    = darray_seg_descriptions_cdata_get(&geom->seg_descriptions);
  sz = darray_seg_descriptions_size_get(&geom->seg_descriptions);
  segments = darray_segment_cdata_get(&geom->unique_segments);
  fprintf(stream, "g %s\n", group_name);
  FOR_EACH(i, 0, sz) {
    int dump;
    if(partition == SG2D_OBJ_DUMP_VALID_PRIMITIVE)
      dump = !(seg_descriptions[i].merge_conflict
        || seg_descriptions[i].properties_conflict);
    else if(partition == SG2D_OBJ_DUMP_MERGE_CONFLICTS)
      dump = seg_descriptions[i].merge_conflict;
    else {
      ASSERT(partition == SG2D_OBJ_DUMP_PROPERTY_CONFLICTS);
      dump = seg_descriptions[i].properties_conflict;
    }
    if(!dump) continue;
    fprintf(stream, "l "PRTF_SEG" "PRTF_SEG"\n",
      /* OBJ indexing starts at 1 */
      1 + segments[i].vertex_ids[0],
      1 + segments[i].vertex_ids[1]);
  }
}

/******************************************************************************
 * Exported functions
 *****************************************************************************/
res_T
sg2d_geometry_create
  (struct sg2d_device* dev,
   struct sg2d_geometry** out_geometry)
{
  struct sg2d_geometry* geom = NULL;
  res_T res = RES_OK;

  if(!dev || !out_geometry) {
    res = RES_BAD_ARG;
    goto error;
  }

  geom = MEM_CALLOC(dev->allocator, 1, sizeof(struct sg2d_geometry));
  if(!geom) {
    log_err(dev,
        "%s: could not allocate the sg2.\n", FUNC_NAME);
    res = RES_MEM_ERR;
    goto error;
  }

  SG2D(device_ref_get(dev));
  darray_segment_init(dev->allocator, &geom->unique_segments);
  darray_vertex_init(dev->allocator, &geom->unique_vertices);
  htable_seg_init(dev->allocator, &geom->unique_segments_ids);
  htable_vrtx_init(dev->allocator, &geom->unique_vertices_ids);
  darray_seg_descriptions_init(dev->allocator, &geom->seg_descriptions);
  geom->segment_count_including_duplicates = 0;
  geom->sides_with_defined_medium_count = 0;
  geom->set_id = 0;
  geom->seg_with_unspecified_sides_count = 0;
  geom->seg_with_unspecified_intface_count = 0;
  geom->merge_conflict_count = 0;
  geom->properties_conflict_count = 0;
  geom->dev = dev;

  ref_init(&geom->ref);

exit:
  if(out_geometry) *out_geometry = geom;
  return res;
error:
  if(geom) {
    SG2D(geometry_ref_put(geom));
    geom = NULL;
  }
  goto exit;
}

res_T
sg2d_geometry_reserve
  (struct sg2d_geometry* geom,
   const vrtx_id_t vertices_count,
   const seg_id_t segments_count,
   const prop_id_t properties_count)
{
  res_T res = RES_OK;
  if(!geom) return RES_BAD_ARG;

  ERR(darray_segment_reserve(&geom->unique_segments, segments_count));
  ERR(darray_vertex_reserve(&geom->unique_vertices, vertices_count));
  ERR(htable_vrtx_reserve(&geom->unique_vertices_ids, vertices_count));
  ERR(htable_seg_reserve(&geom->unique_segments_ids, segments_count));
  ERR(darray_seg_descriptions_reserve(&geom->seg_descriptions,
    properties_count));

end:
  return res;
error:
  goto end;
}

res_T
sg2d_geometry_add
  (struct sg2d_geometry* geom,
   const vrtx_id_t nverts,
   const seg_id_t nsegs,
   const struct sg2d_geometry_add_callbacks* callbacks,
   void* ctx) /* Can be NULL */
{
  res_T res = RES_OK;
  struct mem_allocator* alloc;
  size_t nusegs, nuverts;
  vrtx_id_t nv, n_new_uverts = 0;
  seg_id_t ns, n_new_usegs = 0;
  struct segment* seg;
  /* Tmp table of IDs to record unique IDs of the currently added vertices */
  struct darray_vertice_ids unique_vertice_ids;
  int unique_vertice_ids_initialized = 0;
  get_indices_t get_ind;
  get_properties_t get_prop;
  get_position_t get_pos;
  add_segment_t add_seg;
  merge_segment_t mrg_seg;
  degenerated_segment_t dege_seg;

  if(!geom || !callbacks || !callbacks->get_indices || !callbacks->get_position)
  {
    res = RES_BAD_ARG;
    goto error;
  }

  get_ind = callbacks->get_indices;
  get_prop = callbacks->get_properties;
  get_pos = callbacks->get_position;
  add_seg = callbacks->add_segment;
  mrg_seg = callbacks->merge_segment;
  dege_seg = callbacks->degenerated_segment;
  alloc = geom->dev->allocator;
  nuverts = darray_vertex_size_get(&geom->unique_vertices);
  nusegs = darray_segment_size_get(&geom->unique_segments);

  /* Make room for new geometry; suppose no more duplicates */
  darray_vertice_ids_init(alloc, &unique_vertice_ids);
  unique_vertice_ids_initialized = 1;
  ERR(darray_vertice_ids_reserve(&unique_vertice_ids, nverts));
  ERR(darray_vertex_reserve(&geom->unique_vertices, nuverts + nverts));
  ERR(darray_segment_reserve(&geom->unique_segments, nusegs + nsegs));
  ERR(htable_vrtx_reserve(&geom->unique_vertices_ids, nuverts + nverts));
  ERR(htable_seg_reserve(&geom->unique_segments_ids, nusegs + nsegs));
  ASSERT(nusegs == darray_seg_descriptions_size_get(&geom->seg_descriptions));
  ERR(darray_seg_descriptions_reserve(&geom->seg_descriptions, nusegs + nsegs));
  /* Get vertices and deduplicate */
  FOR_EACH(nv, 0, nverts) {
    vrtx_id_t* p_vrtx;
    struct vertex tmp;
    vrtx_id_t v_idx;
    get_pos(nv, tmp.coord, ctx);
    p_vrtx = htable_vrtx_find(&geom->unique_vertices_ids, &tmp);
    if(p_vrtx) {
      /* Duplicate vertex */
      v_idx = *p_vrtx;
    } else {
      /* New vertex */
      ASSERT(nuverts + n_new_uverts <= VRTX_MAX__);
      v_idx = (vrtx_id_t)(nuverts + n_new_uverts);
      ASSERT(v_idx == htable_vrtx_size_get(&geom->unique_vertices_ids));
      ERR(darray_vertex_push_back(&geom->unique_vertices, &tmp));
      ERR(htable_vrtx_set(&geom->unique_vertices_ids, &tmp, &v_idx));
      ++n_new_uverts;
    }
    /* Keep the unique ID for vertex nv */
    ERR(darray_vertice_ids_push_back(&unique_vertice_ids, &v_idx));
  }

  /* Get segments and deduplicate */
  seg = darray_segment_data_get(&geom->unique_segments);
  FOR_EACH(ns, 0, nsegs) {
    int j, reversed;
    struct vrtx_id2 seg_key;
    struct segment tmp = SEG_UNDEF__;
    seg_id_t* p_seg;
    struct seg_descriptions* seg_descriptions = NULL;
    seg_id_t unique_id;

    get_ind(ns, tmp.vertex_ids, ctx);
    FOR_EACH(j, 0, 2) {
      if(tmp.vertex_ids[j] >= nverts) {
        res = RES_BAD_ARG;
        goto error;
      }
      /* Replace the vertex ID by its the unique ID */
      tmp.vertex_ids[j]
        = darray_vertice_ids_cdata_get(&unique_vertice_ids)[tmp.vertex_ids[j]];
    }
    if(tmp.vertex_ids[0] == tmp.vertex_ids[1]) {
      int abort = 0;
      if(dege_seg) {
        /* Let the client app rule. */
        ERR(dege_seg(ns, ctx, &abort));
      } else {
        log_warn(geom->dev, "%s: segment "PRTF_SEG" is degenerated.\n",
          FUNC_NAME, ns);
      }
      if(abort) {
        res = RES_BAD_ARG;
        goto error;
      }
      else continue;
    }
#ifndef NDEBUG
    {
      double edge[2];
      const struct vertex* vertices
        = darray_vertex_cdata_get(&geom->unique_vertices);
      d2_sub(edge, vertices[tmp.vertex_ids[0]].coord,
        vertices[tmp.vertex_ids[1]].coord);
      /* As vertices are deduplicated, an edge cannot be nul if vertices have
       * distinct indices */
      ASSERT(d2_len(edge) != 0);
    }
#endif
    /* Get properties */
    if(get_prop) get_prop(ns, tmp.properties, ctx);
    /* Find duplicate segments */
    reversed = seg_make_key(&seg_key, tmp.vertex_ids);
    p_seg = htable_seg_find(&geom->unique_segments_ids, &seg_key);
    if(p_seg) {
      /* Duplicate segment. Need to check duplicate validity */
      struct vrtx_id2 useg_key;
      int ureversed = seg_make_key(&useg_key, seg[*p_seg].vertex_ids);
      int same = (reversed == ureversed);
      int already_conflict;
      ASSERT(seg_key_eq(&seg_key, &useg_key));
      unique_id = *p_seg;
      ERR(geometry_enlarge_seg_descriptions(geom, 1 + unique_id));
      seg_descriptions
        = darray_seg_descriptions_data_get(&geom->seg_descriptions);
      if(!same)
        SWAP(prop_id_t, tmp.properties[SG2D_FRONT], tmp.properties[SG2D_BACK]);
      already_conflict = seg_descriptions[ns].merge_conflict;
      if(mrg_seg) {
        /* Let the client app rule. */
        ERR(mrg_seg(unique_id, ns, !same, seg[*p_seg].properties,
          tmp.properties, ctx, &seg_descriptions[ns].merge_conflict));
      } else {
        FOR_EACH(j, 0, SG2D_PROP_TYPES_COUNT__) {
          if(!sg2d_compatible_property(seg[*p_seg].properties[j],
            tmp.properties[j]))
          {
            seg_descriptions[ns].merge_conflict = 1;
            break;
          }
        }
      }
      if(seg_descriptions[ns].merge_conflict && !already_conflict)
        geom->merge_conflict_count++;
      /* Replace SG2D_UNSPECIFIED_PROPERTY properties */
      FOR_EACH(j, 0, SG2D_PROP_TYPES_COUNT__) {
        if(seg[*p_seg].properties[j] == SG2D_UNSPECIFIED_PROPERTY
          && tmp.properties[j] != SG2D_UNSPECIFIED_PROPERTY) {
          seg[*p_seg].properties[j] = tmp.properties[j];
          if(j == SG2D_FRONT || j == SG2D_BACK)
            geom->sides_with_defined_medium_count++;
        }
      }
    } else {
      /* New segment */
      ASSERT(nusegs + n_new_usegs <= SEG_MAX__);
      unique_id = (seg_id_t)(nusegs + n_new_usegs);
      tmp.user_id = geom->segment_count_including_duplicates + ns;
      if(add_seg) ERR(add_seg(unique_id, ns, ctx));

      ERR(geometry_enlarge_seg_descriptions(geom, 1 + unique_id));
      seg_descriptions
        = darray_seg_descriptions_data_get(&geom->seg_descriptions);
      ERR(darray_segment_push_back(&geom->unique_segments, &tmp));
      FOR_EACH(j, 0, SG2D_PROP_TYPES_COUNT__) {
        if((j == SG2D_FRONT || j == SG2D_BACK)
          && tmp.properties[j] != SG2D_UNSPECIFIED_PROPERTY)
          geom->sides_with_defined_medium_count++;
      }
      ASSERT(unique_id == htable_seg_size_get(&geom->unique_segments_ids));
      ERR(htable_seg_set(&geom->unique_segments_ids, &seg_key, &unique_id));
      n_new_usegs++;
    }
    ERR(geometry_register_segment(geom, &tmp, unique_id, geom->set_id,
      seg_descriptions[ns].properties_conflict));
    if(seg_descriptions[ns].properties_conflict)
      geom->merge_conflict_count++;
  }

  ASSERT(nuverts + n_new_uverts
    == htable_vrtx_size_get(&geom->unique_vertices_ids));
  ASSERT(nusegs + n_new_usegs
    == htable_seg_size_get(&geom->unique_segments_ids));
exit:
  if(geom) {
    geom->set_id++;
    geom->segment_count_including_duplicates += nsegs;
  }
  if(unique_vertice_ids_initialized)
    darray_vertice_ids_release(&unique_vertice_ids);
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_validate_properties
  (struct sg2d_geometry* geom,
   res_T(*validate)(const seg_id_t, const prop_id_t*, void*, int*),
   void* ctx)
{
  size_t sz__;
  seg_id_t i, sz;
  struct seg_descriptions* seg_descriptions;
  res_T res = RES_OK;

  if(!geom || !validate) {
    res = RES_BAD_ARG;
    goto error;
  }

  sz__ = darray_seg_descriptions_size_get(&geom->seg_descriptions);
  ASSERT(sz__ <= SEG_MAX__);
  sz = (seg_id_t)sz__;
  seg_descriptions
    = darray_seg_descriptions_data_get(&geom->seg_descriptions);
  geom->properties_conflict_count = 0; /* Reset count */
  FOR_EACH(i, 0, sz) {
    int p;
    prop_id_t props[SG2D_PROP_TYPES_COUNT__];
    struct seg_descriptions* segd = seg_descriptions + i;
    /* Validate only segment not flagged with merge_conflict */
    if(segd->merge_conflict) {
      segd->properties_conflict = 0;
      continue;
    }
    /* Get properties for non-conflict segments */
    FOR_EACH(p, 0, SG2D_PROP_TYPES_COUNT__) {
      size_t j;
      const struct definition* defs = darray_definition_cdata_get(segd->defs + p);
      props[p] = SG2D_UNSPECIFIED_PROPERTY;
      FOR_EACH(j, 0, darray_definition_size_get(segd->defs + p)) {
        if(defs[j].property_value != SG2D_UNSPECIFIED_PROPERTY) {
          props[p] = defs[j].property_value;
          break;
        }
      }
    }
    /* Call vaildation */
    ERR(validate(i, props, ctx, &segd->properties_conflict));
    if(segd->properties_conflict)
      geom->properties_conflict_count++;
  }

exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_vertices_count
  (const struct sg2d_geometry* geom,
   vrtx_id_t* count)
{
  res_T res = RES_OK;
  size_t sz;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  sz = darray_vertex_size_get(&geom->unique_vertices);
  ASSERT(sz <= VRTX_MAX__);
  *count = (vrtx_id_t)sz;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_vertex
  (const struct sg2d_geometry* geom,
   const vrtx_id_t ivtx,
   double coord[SG2D_GEOMETRY_DIMENSION])
{
  res_T res = RES_OK;
  const struct vertex* vertices;
  if(!geom || !coord
    || ivtx >= darray_vertex_size_get(&geom->unique_vertices))
  {
    res = RES_BAD_ARG;
    goto error;
  }
  vertices = darray_vertex_cdata_get(&geom->unique_vertices);
  d2_set(coord, vertices[ivtx].coord);
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_added_segments_count
  (const struct sg2d_geometry* geom,
   seg_id_t* count)
{
  res_T res = RES_OK;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  *count = geom->segment_count_including_duplicates;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segments_count
  (const struct sg2d_geometry* geom,
   seg_id_t* count)
{
  res_T res = RES_OK;
  size_t sz;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  sz = darray_segment_size_get(&geom->unique_segments);
  ASSERT(sz <= SEG_MAX__);
  *count = (seg_id_t)sz;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segment_vertices
  (const struct sg2d_geometry* geom,
   const seg_id_t iseg,
   vrtx_id_t indices[SG2D_GEOMETRY_DIMENSION])
{
  res_T res = RES_OK;
  const struct segment* segments;
  size_t i;
  if(!geom || !indices
    || iseg >= darray_segment_size_get(&geom->unique_segments))
  {
    res = RES_BAD_ARG;
    goto error;
  }
  segments = darray_segment_cdata_get(&geom->unique_segments);
  FOR_EACH(i, 0, SG2D_GEOMETRY_DIMENSION)
    indices[i] = segments[iseg].vertex_ids[i];
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segment_properties
  (const struct sg2d_geometry* geom,
   const seg_id_t iseg,
   prop_id_t properties[SG2D_PROP_TYPES_COUNT__])
{
  res_T res = RES_OK;
  const struct segment* segments;
  size_t i;
  if(!geom || !properties
    || iseg >= darray_segment_size_get(&geom->unique_segments))
  {
    res = RES_BAD_ARG;
    goto error;
  }
  segments = darray_segment_cdata_get(&geom->unique_segments);
  FOR_EACH(i, 0, SG2D_PROP_TYPES_COUNT__)
    properties[i] = segments[iseg].properties[i];
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segment_user_id
  (const struct sg2d_geometry* geom,
   const seg_id_t iseg,
   seg_id_t* user_id)
{
  res_T res = RES_OK;
  const struct segment* segments;
  if(!geom || !user_id
    || iseg >= darray_segment_size_get(&geom->unique_segments))
  {
    res = RES_BAD_ARG;
    goto error;
  }
  segments = darray_segment_cdata_get(&geom->unique_segments);
  *user_id = segments[iseg].user_id;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segments_with_unspecified_side_count
  (const struct sg2d_geometry* geom,
   seg_id_t* count)
{
  res_T res = RES_OK;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  *count = geom->seg_with_unspecified_sides_count;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segments_with_unspecified_interface_count
  (const struct sg2d_geometry* geom,
   seg_id_t* count)
{
  res_T res = RES_OK;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  *count = geom->seg_with_unspecified_intface_count;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segments_with_merge_conflict_count
  (const struct sg2d_geometry* geom,
   seg_id_t* count)
{
  res_T res = RES_OK;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  *count = geom->merge_conflict_count;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_get_unique_segments_with_properties_conflict_count
  (const struct sg2d_geometry* geom,
   seg_id_t* count)
{
  res_T res = RES_OK;
  if(!geom || !count) {
    res = RES_BAD_ARG;
    goto error;
  }
  *count = geom->properties_conflict_count;
exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_dump_as_obj
  (const struct sg2d_geometry* geom,
   FILE* stream,
   const int flags)
{
  res_T res = RES_OK;
  const struct vertex* vertices;
  size_t vsz, ssz, i;
  if(!geom || !stream || !flags
    || !geom->segment_count_including_duplicates)
  {
    if(geom && !geom->segment_count_including_duplicates)
      log_err(geom->dev,
        "%s: cannot dump empty geometries as OBJ\n",
        FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }
  /* Headers */
  fprintf(stream, "# Dump of star-geometry-2d\n");
  fprintf(stream, "# Geometry counts:\n");
  vsz = darray_vertex_size_get(&geom->unique_vertices);
  ASSERT(vsz <= VRTX_MAX__);
  fprintf(stream, "# . "PRTF_VRTX" vertices\n", (vrtx_id_t)vsz);
  ssz = darray_segment_size_get(&geom->unique_segments);
  ASSERT(ssz <= SEG_MAX__);
  fprintf(stream, "# . "PRTF_SEG" segments\n", (seg_id_t)ssz);
  fprintf(stream,
    "# . "PRTF_SEG" segments flagged with a merge conflict\n",
    geom->merge_conflict_count);
  fprintf(stream,
    "# . "PRTF_SEG" segments flagged with a property conflict\n",
    geom->merge_conflict_count);

  /* Dump vertices */
  vertices = darray_vertex_cdata_get(&geom->unique_vertices);
  FOR_EACH(i, 0, vsz)
    fprintf(stream, "v %g %g 0\n", SPLIT2(vertices[i].coord));

  /* Dump segments by groups */
  dump_partition(geom, stream,
    "Valid_segments", SG2D_OBJ_DUMP_VALID_PRIMITIVE);
  dump_partition(geom, stream,
    "Merge_conflicts", SG2D_OBJ_DUMP_MERGE_CONFLICTS);
  dump_partition(geom, stream,
    "Property_conflicts", SG2D_OBJ_DUMP_PROPERTY_CONFLICTS);

exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_dump_as_vtk
  (const struct sg2d_geometry* geom,
   FILE* stream)
{
  res_T res = RES_OK;
  const struct vertex* vertices;
  const struct segment* segments;
  const struct seg_descriptions* descriptions;
  size_t vsz, ssz, i;
  if(!geom || !stream || !geom->segment_count_including_duplicates) {
    if(geom && !geom->segment_count_including_duplicates)
      log_err(geom->dev,
        "%s: cannot dump empty geometries as VTK\n",
        FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }
  /* Headers */
  fprintf(stream, "# vtk DataFile Version 2.0\n");
  fprintf(stream, "Dump of star-geometry-2d geometry\n");
  fprintf(stream, "ASCII\n");
  fprintf(stream, "DATASET POLYDATA\n");

  /* Dump vertices */
  vsz = darray_vertex_size_get(&geom->unique_vertices);
  ASSERT(vsz <= VRTX_MAX__);
  fprintf(stream, "POINTS "PRTF_VRTX" double\n", (vrtx_id_t)vsz);
  vertices = darray_vertex_cdata_get(&geom->unique_vertices);
  FOR_EACH(i, 0, vsz)
    fprintf(stream, "%g %g 0\n", SPLIT2(vertices[i].coord));

  /* Dump segments */
  ssz = darray_segment_size_get(&geom->unique_segments);
  ASSERT(3 * ssz <= SEG_MAX__);
  fprintf(stream, "LINES "PRTF_SEG" "PRTF_SEG"\n",
    (seg_id_t)ssz, (seg_id_t)(3 * ssz));
  segments = darray_segment_cdata_get(&geom->unique_segments);
  FOR_EACH(i, 0, ssz)
    fprintf(stream, "2 "PRTF_VRTX" "PRTF_VRTX"\n",
      SPLIT2(segments[i].vertex_ids));

  /* Start segments properties */
  fprintf(stream, "CELL_DATA "PRTF_SEG"\n", (seg_id_t)ssz);
  descriptions = darray_seg_descriptions_cdata_get(&geom->seg_descriptions);

  /* Dump front medium */
  fprintf(stream, "SCALARS Front_medium unsigned_int 1\n");
  fprintf(stream, "LOOKUP_TABLE default\n");
  dump_seg_property(geom, stream, SG2D_FRONT);

  /* Dump back medium */
  fprintf(stream, "SCALARS Back_medium unsigned_int 1\n");
  fprintf(stream, "LOOKUP_TABLE default\n");
  dump_seg_property(geom, stream, SG2D_BACK);

  /* Dump interface */
  fprintf(stream, "SCALARS Interface unsigned_int 1\n");
  fprintf(stream, "LOOKUP_TABLE default\n");
  dump_seg_property(geom, stream, SG2D_INTFACE);

  /* Dump unique_id */
  fprintf(stream, "SCALARS Unique_ID unsigned_int 1\n");
  fprintf(stream, "LOOKUP_TABLE default\n");
  FOR_EACH(i, 0, ssz) fprintf(stream, PRTF_SEG"\n", (seg_id_t)i);

  /* Dump user_id */
  fprintf(stream, "SCALARS User_ID unsigned_int 1\n");
  fprintf(stream, "LOOKUP_TABLE default\n");
  FOR_EACH(i, 0, ssz) fprintf(stream, PRTF_SEG"\n", segments[i].user_id);

  /* Dump merge conflict status (if any) */
  if(geom->merge_conflict_count) {
    fprintf(stream, "SCALARS Merge_conflict int\n");
    fprintf(stream, "LOOKUP_TABLE default\n");
    FOR_EACH(i, 0, ssz)
      fprintf(stream, "%d\n", descriptions[i].merge_conflict);
  }

  /* Dump property conflict status (if any) */
  if(geom->properties_conflict_count) {
    fprintf(stream, "SCALARS Property_conflict int\n");
    fprintf(stream, "LOOKUP_TABLE default\n");
    FOR_EACH(i, 0, ssz)
      fprintf(stream, "%d\n", descriptions[i].properties_conflict);
  }

  /* Dump rank of the sg2d_geometry_add that created the segment */
  fprintf(stream, "SCALARS Created_at_sg2d_geometry_add unsigned_int 1\n");
  fprintf(stream, "LOOKUP_TABLE default\n");
  FOR_EACH(i, 0, ssz) {
    const struct definition* tdefs;
    const unsigned* ranks;
    ASSERT(darray_definition_size_get(&descriptions[i].defs[SG2D_FRONT]) > 0);
    /* Rank is the first set_id of the first definition of any property */
    tdefs = darray_definition_cdata_get(&descriptions[i].defs[SG2D_FRONT]);
    ranks = darray_uint_cdata_get(&tdefs[0].set_ids);
    fprintf(stream, "%u\n", ranks[0]);
  }

exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_dump_as_c_code
  (const struct sg2d_geometry* geom,
   FILE* stream,
   const char* name_prefix,
   const int flags)
{
  res_T res = RES_OK;
  const struct vertex* vertices;
  const struct segment* segments;
  const char* qualifiers;
  size_t vsz, ssz, i;
  if(!geom || !stream
    || geom->merge_conflict_count
    || geom->properties_conflict_count
    || !geom->segment_count_including_duplicates)
  {
    if(geom
      && (geom->merge_conflict_count
        || geom->properties_conflict_count))
      log_err(geom->dev,
        "%s: cannot dump geometries with conflict as C code\n",
        FUNC_NAME);
    if(geom && !geom->segment_count_including_duplicates)
      log_err(geom->dev,
        "%s: cannot dump empty geometries as C code\n",
        FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!name_prefix) name_prefix = "";
  /* Headers */
  if(name_prefix && name_prefix[0] != '\0')
    fprintf(stream, "/* Dump of star-geometry-2d '%s'. */\n", name_prefix);
  else
    fprintf(stream, "/* Dump of star-geometry-2d. */\n");
  vsz = darray_vertex_size_get(&geom->unique_vertices);
  ASSERT(2 * vsz <= VRTX_MAX__);
  ssz = darray_segment_size_get(&geom->unique_segments);
  ASSERT(2 * ssz <= SEG_MAX__);

  if(vsz == 0 || ssz == 0) {
    log_err(geom->dev,
      "%s: no geometry to dump\n",
      FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  }

  if(flags & SG2D_C_DUMP_CONST && flags & SG2D_C_DUMP_STATIC)
    qualifiers = "static const ";
  else if(flags & SG2D_C_DUMP_CONST)
    qualifiers = "const ";
  else if(flags & SG2D_C_DUMP_STATIC)
    qualifiers = "static ";
  else qualifiers = "";

  /* Dump vertices */
  fprintf(stream, "%s"VRTX_TYPE_NAME" %s_vertices_count = "PRTF_VRTX";\n",
    qualifiers, name_prefix, (vrtx_id_t)vsz);

  vertices = darray_vertex_cdata_get(&geom->unique_vertices);
  fprintf(stream,
    "%sdouble %s_vertices["PRTF_VRTX"] =\n"
    "{\n",
    qualifiers, name_prefix, (vrtx_id_t)(2 * vsz));
  FOR_EACH(i, 0, vsz - 1)
    fprintf(stream,
      "   %g, %g,\n", SPLIT2(vertices[i].coord));
  fprintf(stream,
    "   %g, %g\n", SPLIT2(vertices[vsz - 1].coord));
  fprintf(stream,
    "};\n");

  /* Dump segments */
  fprintf(stream, "%s"SEG_TYPE_NAME" %s_segments_count = "PRTF_SEG";\n",
    qualifiers, name_prefix, (seg_id_t)ssz);

  segments = darray_segment_cdata_get(&geom->unique_segments);
  fprintf(stream,
    "%s"SEG_TYPE_NAME" %s_segments["PRTF_SEG"] =\n"
    "{\n",
    qualifiers, name_prefix, (seg_id_t)(2 * ssz));
  FOR_EACH(i, 0, ssz - 1)
    fprintf(stream,
      "   "PRTF_VRTX", "PRTF_VRTX",\n", SPLIT2(segments[i].vertex_ids));
  fprintf(stream,
    "   "PRTF_VRTX", "PRTF_VRTX"\n", SPLIT2(segments[ssz - 1].vertex_ids));
  fprintf(stream,
    "};\n");

  /* Dump properties */
  fprintf(stream,
    "%s"PROP_TYPE_NAME" %s_properties["PRTF_PROP"] =\n"
    "{\n",
    qualifiers, name_prefix, (prop_id_t)(SG2D_PROP_TYPES_COUNT__ * ssz));
  FOR_EACH(i, 0, ssz) {
    int p;
    fprintf(stream, "  ");
    FOR_EACH(p, 0, SG2D_PROP_TYPES_COUNT__) {
      if(segments[i].properties[p] == SG2D_UNSPECIFIED_PROPERTY)
        fprintf(stream, " SG2D_UNSPECIFIED_PROPERTY");
      else fprintf(stream," "PRTF_PROP, segments[i].properties[p]);
      if(i < ssz-1 || p < 2) fprintf(stream, ",");
      if(p == 2) fprintf(stream, "\n");
    }
  }
  fprintf(stream,
    "};\n");

exit:
  return res;
error:
  goto exit;
}

res_T
sg2d_geometry_ref_get(struct sg2d_geometry* geom)
{
  if(!geom) return RES_BAD_ARG;
  ref_get(&geom->ref);
  return RES_OK;
}

res_T
sg2d_geometry_ref_put(struct sg2d_geometry* geom)
{
  if(!geom) return RES_BAD_ARG;
  ref_put(&geom->ref, geometry_release);
  return RES_OK;
}
