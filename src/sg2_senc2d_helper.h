/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SG2D_SENC_2D_HELPER_H__
#define SG2D_SENC_2D_HELPER_H__

#include "sg2.h"
#include <star/senc2d.h>

#include <rsys/rsys.h>

 /* Get vertex indices for the iseg_th segment.
  * Suitable for use as get_indices callback in senc2d_scene_create calls. */
static FINLINE void
sg2d_senc_geometry_get_indices
  (const unsigned iseg,
   unsigned indices[SG2D_GEOMETRY_DIMENSION],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  res_T r;
  ASSERT(indices && geometry);
  r = sg2d_geometry_get_unique_segment_vertices(geometry, iseg, indices);
  ASSERT(r == RES_OK); (void)r;
}

/* Get vertex indices for the iseg_th segment.
 * Suitable for use as get_media callback in senc2d_scene_create calls. */
static FINLINE void
sg2d_senc_geometry_get_media
  (const unsigned iseg,
   unsigned media[2],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  unsigned tmp[SG2D_PROP_TYPES_COUNT__];
  res_T r;
  ASSERT(media && geometry);
  r = sg2d_geometry_get_unique_segment_properties(geometry, iseg, tmp);
  ASSERT(r == RES_OK); (void)r;
  media[SENC2D_FRONT] = (tmp[SG2D_FRONT] == SG2D_UNSPECIFIED_PROPERTY)
    ? SENC2D_UNSPECIFIED_MEDIUM : tmp[SG2D_FRONT];
  media[SENC2D_BACK] = (tmp[SG2D_BACK] == SG2D_UNSPECIFIED_PROPERTY)
    ? SENC2D_UNSPECIFIED_MEDIUM : tmp[SG2D_BACK];
}

/* Get vertex indices for the iseg_th segment.
 * Suitable for use as get_position callback in senc2d_scene_create calls. */
static FINLINE void
sg2d_senc_geometry_get_position
  (const unsigned ivert,
   double coord[SG2D_GEOMETRY_DIMENSION],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  res_T r;
  ASSERT(coord && geometry);
  r = sg2d_geometry_get_unique_vertex(geometry, ivert, coord);
  ASSERT(r == RES_OK); (void)r;
}

END_DECLS

#endif /* SG2D_SENC_2D_HELPER_H__ */
