/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef TEST_SG2D_UTILS2_H
#define TEST_SG2D_UTILS2_H

#include <rsys/stretchy_array.h>

/******************************************************************************
 * Circle functions
 *****************************************************************************/
static INLINE void
create_circle
  (const double radius,
   const unsigned nslices,
   struct context* ctx)
{
  double step_theta;
  unsigned itheta;
  unsigned islice;
  double* d = NULL;
  unsigned* u = NULL;
  ASSERT(radius > 0 && nslices >= 3 && ctx);

  step_theta = 2 * PI / (double)nslices;
  FOR_EACH(itheta, 0, nslices) {
    const double theta = (double)itheta * step_theta;
    const double x = cos(theta);
    const double y = sin(theta);
    sa_push(d, x * radius);
    sa_push(d, y * radius);
  }
  ctx->positions = d;

  FOR_EACH(islice, 0, nslices) {
    const unsigned v0 = islice;
    const unsigned v1 = ((islice + 1) % nslices);
    sa_push(u, v0);
    sa_push(u, v1);
  }
  ctx->indices = u;
}

static INLINE void
circle_release(struct context* ctx)
{
  ASSERT(ctx);
  sa_release(ctx->positions);
  sa_release(ctx->indices);
  ctx->positions = NULL;
  ctx->indices = NULL;
}

static INLINE void
get_uniform_properties
  (const unsigned iseg,
    unsigned property[SG2D_PROP_TYPES_COUNT__],
   void* context)
{
  const struct context* ctx = context;
  ASSERT(property && ctx); (void)iseg;
  property[ctx->reverse_med ? SG2D_BACK : SG2D_FRONT] = *ctx->front_media;
  property[ctx->reverse_med ? SG2D_FRONT : SG2D_BACK] = *ctx->back_media;
  property[SG2D_INTFACE] = *ctx->intface;
}

#endif /* TEST_SG2D_UTILS2_H */
