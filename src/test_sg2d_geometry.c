/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "sg2d.h"
#include "test_sg2d_utils.h"

#include <stdio.h>

static res_T
validate
  (const unsigned iseg,
   const unsigned properties[SG2D_PROP_TYPES_COUNT__],
   void* context,
   int* properties_conflict)
{
  (void)iseg; (void)properties; (void)context;
  *properties_conflict = 0;
  return RES_OK;
}

static res_T
merge_seg
  (const unsigned user_id,
   const unsigned iseg,
   const int reversed_segment,
   unsigned segment_properties[SG2D_PROP_TYPES_COUNT__],
   const unsigned merged_properties[SG2D_PROP_TYPES_COUNT__],
   void* context,
   int* merge_conflict)
{
  ASSERT(segment_properties && merged_properties && merge_conflict);
  (void)user_id; (void)reversed_segment; (void)context;
  (void)segment_properties; (void)merged_properties; (void)merge_conflict;
  *merge_conflict = (int)iseg;
  return RES_OK;
}

static res_T
degenerated_segment
  (const unsigned iseg,
   void* context,
   int* abort)
{
  struct context* ctx = context;
  ASSERT(abort && ctx);
  (void)iseg;
  *abort = *(int*)ctx->custom;
  return RES_OK;
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct sg2d_device* dev;
  struct sg2d_geometry* geom;
  double coord[2];
  unsigned indices[2];
  unsigned degenerated[2] = { 0, 0 };
  unsigned properties[SG2D_PROP_TYPES_COUNT__];
  struct sg2d_geometry_add_callbacks callbacks = SG2D_ADD_CALLBACKS_NULL__;
  unsigned user_id;
  unsigned count, i;
  struct context ctx = CONTEXT_NULL__;
  (void)argc, (void)argv;
  
  OK(mem_init_proxy_allocator(&allocator, &mem_default_allocator));
  OK(sg2d_device_create(NULL, &allocator, 1, &dev));

  BA(sg2d_geometry_create(NULL, &geom));
  BA(sg2d_geometry_create(dev, NULL));
  OK(sg2d_geometry_create(dev, &geom));

  BA(sg2d_geometry_ref_get(NULL));
  OK(sg2d_geometry_ref_get(geom));

  BA(sg2d_geometry_ref_put(NULL));
  OK(sg2d_geometry_ref_put(geom));
  OK(sg2d_geometry_ref_put(geom));

  OK(sg2d_geometry_create(dev, &geom));

  BA(sg2d_geometry_validate_properties(NULL, NULL, NULL));
  BA(sg2d_geometry_validate_properties(geom, NULL, NULL));
  BA(sg2d_geometry_validate_properties(NULL, validate, NULL));
  OK(sg2d_geometry_validate_properties(geom, validate, NULL));

  BA(sg2d_geometry_get_unique_vertices_count(NULL, NULL));
  BA(sg2d_geometry_get_unique_vertices_count(geom, NULL));
  BA(sg2d_geometry_get_unique_vertices_count(NULL, &count));
  OK(sg2d_geometry_get_unique_vertices_count(geom, &count));

  BA(sg2d_geometry_get_added_segments_count(NULL, NULL));
  BA(sg2d_geometry_get_added_segments_count(geom, NULL));
  BA(sg2d_geometry_get_added_segments_count(NULL, &count));
  OK(sg2d_geometry_get_added_segments_count(geom, &count));

  BA(sg2d_geometry_get_unique_segments_count(NULL, NULL));
  BA(sg2d_geometry_get_unique_segments_count(geom, NULL));
  BA(sg2d_geometry_get_unique_segments_count(NULL, &count));
  OK(sg2d_geometry_get_unique_segments_count(geom, &count));

  BA(sg2d_geometry_get_unique_segments_with_unspecified_side_count(NULL, NULL));
  BA(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, NULL));
  BA(sg2d_geometry_get_unique_segments_with_unspecified_side_count(NULL, &count));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_side_count(geom, &count));

  BA(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(NULL, NULL));
  BA(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, NULL));
  BA(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(NULL, &count));
  OK(sg2d_geometry_get_unique_segments_with_unspecified_interface_count(geom, &count));

  BA(sg2d_geometry_get_unique_segments_with_merge_conflict_count(NULL, NULL));
  BA(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, NULL));
  BA(sg2d_geometry_get_unique_segments_with_merge_conflict_count(NULL, &count));
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));

  BA(sg2d_geometry_get_unique_segments_with_properties_conflict_count(NULL, NULL));
  BA(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, NULL));
  BA(sg2d_geometry_get_unique_segments_with_properties_conflict_count(NULL, &count));
  OK(sg2d_geometry_get_unique_segments_with_properties_conflict_count(geom, &count));

  BA(sg2d_geometry_dump_as_obj(NULL, NULL, 0));
  BA(sg2d_geometry_dump_as_obj(geom, NULL, 0));
  BA(sg2d_geometry_dump_as_obj(NULL, stdout, 0));
  BA(sg2d_geometry_dump_as_obj(NULL, NULL, SG2D_OBJ_DUMP_ALL));
  BA(sg2d_geometry_dump_as_obj(geom, stdout, 0));
  BA(sg2d_geometry_dump_as_obj(geom, NULL, SG2D_OBJ_DUMP_ALL));
  BA(sg2d_geometry_dump_as_obj(NULL, stdout, SG2D_OBJ_DUMP_ALL));
  /* BA because geometry is empty */
  BA(sg2d_geometry_dump_as_obj(geom, stdout, SG2D_OBJ_DUMP_ALL));

  BA(sg2d_geometry_dump_as_vtk(NULL, NULL));
  BA(sg2d_geometry_dump_as_vtk(geom, NULL));
  BA(sg2d_geometry_dump_as_vtk(NULL, stdout));
  /* BA because geometry is empty */
  BA(sg2d_geometry_dump_as_vtk(geom, stdout));

  BA(sg2d_geometry_dump_as_c_code(NULL, NULL, NULL, 0));
  BA(sg2d_geometry_dump_as_c_code(geom, NULL, NULL, 0));
  BA(sg2d_geometry_dump_as_c_code(NULL, stdout, NULL, 0));
  BA(sg2d_geometry_dump_as_c_code(NULL, NULL, "test", 0));
  BA(sg2d_geometry_dump_as_c_code(geom, NULL, "test", 0));
  BA(sg2d_geometry_dump_as_c_code(NULL, stdout, "test", 0));
  /* BA because geometry is empty */
  BA(sg2d_geometry_dump_as_c_code(geom, stdout, NULL, 0));
  BA(sg2d_geometry_dump_as_c_code(geom, stdout, "test", 0));

  BA(sg2d_geometry_add(NULL, 0, 0, &callbacks, NULL));
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == 0);
  BA(sg2d_geometry_add(geom, nvertices, nsegments, NULL, NULL));
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments);
  /* Mandatory callbacks are NULL */
  callbacks.get_indices = NULL;
  callbacks.get_position = get_position;
  BA(sg2d_geometry_add(geom, 0, 0, &callbacks, NULL));
  callbacks.get_indices = get_indices;
  callbacks.get_position = NULL;
  BA(sg2d_geometry_add(geom, 0, 0, &callbacks, NULL));
  callbacks.get_indices = NULL;
  callbacks.get_position = NULL;
  BA(sg2d_geometry_add(geom, 0, 0, &callbacks, NULL));
  /* Add 0 items */
  callbacks.get_indices = get_indices;
  callbacks.get_position = get_position;
  OK(sg2d_geometry_add(geom, 0, 0, &callbacks, NULL));
  OK(sg2d_geometry_get_added_segments_count(geom, &count));
  CHK(count == nsegments);

  /* A 2D square.
   * 2 enclosures (inside, outside) sharing the same segments,
   * but opposite sides */
  ctx.positions = square_vertices;
  ctx.indices = box_indices;
  ctx.front_media = medium0;
  ctx.back_media = medium1;
  ctx.intface = intface0;

  callbacks.get_indices = get_indices;
  callbacks.get_properties = get_properties;
  callbacks.get_position = get_position;

  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_dump_as_obj(geom, stdout, SG2D_OBJ_DUMP_ALL));
  OK(sg2d_geometry_dump_as_vtk(geom, stdout));
  OK(sg2d_geometry_dump_as_c_code(geom, stdout, NULL, 0));
  OK(sg2d_geometry_dump_as_c_code(geom, stdout, "test",
    SG2D_C_DUMP_STATIC | SG2D_C_DUMP_CONST));

  BA(sg2d_geometry_get_unique_vertex(NULL, nsegments, NULL));
  BA(sg2d_geometry_get_unique_vertex(geom, nsegments, NULL));
  BA(sg2d_geometry_get_unique_vertex(NULL, 0, NULL));
  BA(sg2d_geometry_get_unique_vertex(NULL, nsegments, coord));
  BA(sg2d_geometry_get_unique_vertex(geom, 0, NULL));
  BA(sg2d_geometry_get_unique_vertex(geom, nsegments, coord));
  BA(sg2d_geometry_get_unique_vertex(NULL, 0, coord));
  OK(sg2d_geometry_get_unique_vertex(geom, 0, coord));

  BA(sg2d_geometry_get_unique_segment_vertices(NULL, nsegments, NULL));
  BA(sg2d_geometry_get_unique_segment_vertices(geom, nsegments, NULL));
  BA(sg2d_geometry_get_unique_segment_vertices(NULL, 0, NULL));
  BA(sg2d_geometry_get_unique_segment_vertices(NULL, nsegments, indices));
  BA(sg2d_geometry_get_unique_segment_vertices(geom, 0, NULL));
  BA(sg2d_geometry_get_unique_segment_vertices(geom, nsegments, indices));
  BA(sg2d_geometry_get_unique_segment_vertices(NULL, 0, indices));
  OK(sg2d_geometry_get_unique_segment_vertices(geom, 0, indices));
  FOR_EACH(i, 0 , 2) CHK(indices[i] == box_indices[i]);

  BA(sg2d_geometry_get_unique_segment_properties(NULL, nsegments, NULL));
  BA(sg2d_geometry_get_unique_segment_properties(geom, nsegments, NULL));
  BA(sg2d_geometry_get_unique_segment_properties(NULL, 0, NULL));
  BA(sg2d_geometry_get_unique_segment_properties(NULL, nsegments, properties));
  BA(sg2d_geometry_get_unique_segment_properties(geom, 0, NULL));
  BA(sg2d_geometry_get_unique_segment_properties(geom, nsegments, properties));
  BA(sg2d_geometry_get_unique_segment_properties(NULL, 0, properties));
  OK(sg2d_geometry_get_unique_segment_properties(geom, 0, properties));
  CHK(medium0[0] == properties[SG2D_FRONT]);
  CHK(medium1[0] == properties[SG2D_BACK]);
  CHK(intface0[0] == properties[SG2D_INTFACE]);

  BA(sg2d_geometry_get_unique_segment_user_id(NULL, nsegments, NULL));
  BA(sg2d_geometry_get_unique_segment_user_id(geom, nsegments, NULL));
  BA(sg2d_geometry_get_unique_segment_user_id(NULL, 0, NULL));
  BA(sg2d_geometry_get_unique_segment_user_id(NULL, nsegments, &user_id));
  BA(sg2d_geometry_get_unique_segment_user_id(geom, 0, NULL));
  BA(sg2d_geometry_get_unique_segment_user_id(geom, nsegments, &user_id));
  BA(sg2d_geometry_get_unique_segment_user_id(NULL, 0, &user_id));
  OK(sg2d_geometry_get_unique_segment_user_id(geom, 0, &user_id));
  /* Due to a failed attempt to add nsegments segments, user_id for the
   * first successfully added segment is shifted */
  CHK(user_id == nsegments);

  /* Conflicts with merge_seg callback */
  callbacks.merge_segment = merge_seg;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  /* Due to merge_seg internals, all but the first segment have conflict */
  CHK(count == nsegments - 1);
  OK(sg2d_geometry_dump_as_obj(geom, stdout, SG2D_OBJ_DUMP_ALL));
  OK(sg2d_geometry_dump_as_vtk(geom, stdout));
  /* BA because of conflicts */
  BA(sg2d_geometry_dump_as_c_code(geom, stdout, "test", SG2D_C_DUMP_STATIC));
  OK(sg2d_geometry_ref_put(geom));

  /* Conflicts without merge_seg callback */
  OK(sg2d_geometry_create(dev, &geom));
  callbacks.merge_segment = NULL;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  ctx.front_media = medium1_top0;
  OK(sg2d_geometry_add(geom, nvertices, nsegments, &callbacks, &ctx));
  OK(sg2d_geometry_get_unique_segments_with_merge_conflict_count(geom, &count));
  FOR_EACH(i, 0, nsegments) if(medium0[i] != medium1_top0[i]) count--;
  CHK(count == 0);
  OK(sg2d_geometry_dump_as_obj(geom, stdout, SG2D_OBJ_DUMP_ALL));
  OK(sg2d_geometry_dump_as_vtk(geom, stdout));
  /* BA because of conflicts */
  BA(sg2d_geometry_dump_as_c_code(geom, stdout, "test", SG2D_C_DUMP_CONST));

  /* Degenerated segments */
  ctx.indices = degenerated;
  /* Without callback : OK */
  OK(sg2d_geometry_add(geom, nvertices, 1, &callbacks, &ctx));
  /* With callback : OK */
  callbacks.degenerated_segment = degenerated_segment;
  ctx.custom = &i;
  i = 0;
  OK(sg2d_geometry_add(geom, nvertices, 1, &callbacks, &ctx));
  /* With callback : KO */
  i= 1;
  BA(sg2d_geometry_add(geom, nvertices, 1, &callbacks, &ctx));

  OK(sg2d_geometry_ref_put(geom));
  OK(sg2d_device_ref_put(dev));

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHK(mem_allocated_size() == 0);
  return 0;
}
