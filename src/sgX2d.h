/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef STAR_GEOMETRY2D_X_H__
#define STAR_GEOMETRY2D_X_H__

#if !defined(SGXD_DIM) || (SGXD_DIM != 2 && SGXD_DIM != 3)
#error "SGXD_DIM must be defined; admissible values are 2 and 3"
#endif

#include <star/sg2d.h>

/* Star-geometry-XD macros generic to the SGXD_DIM */
#ifndef SGXD
#define SGXD CONCAT(CONCAT(SGX, SGXD_DIM), D)
#endif
#ifndef sgXd
#define sgXd(Name) CONCAT(CONCAT(CONCAT(sg, SGXD_DIM), d_), Name)
#endif
#ifndef SGXD_
#define SGXD_(Name) CONCAT(CONCAT(CONCAT(SGX, SGXD_DIM), D_), Name)
#endif

/* Function names that require dedicated macros */
#define sgXd_geometry_get_added_primitives_count \
  sg2d_geometry_get_added_segments_count
#define sgXd_geometry_get_unique_primitives_count \
  sg2d_geometry_get_unique_segments_count
#define sgXd_geometry_get_unique_primitive_vertices \
  sg2d_geometry_get_unique_segment_vertices
#define sgXd_geometry_get_unique_primitive_properties \
  sg2d_geometry_get_unique_segment_properties
#define sgXd_geometry_get_unique_primitive_user_id \
  sg2d_geometry_get_unique_segment_user_id
#define sgXd_geometry_get_unique_primitives_with_unspecified_side_count \
  sg2d_geometry_get_unique_segments_with_unspecified_side_count
#define sgXd_geometry_get_unique_primitives_with_unspecified_interface_count \
  sg2d_geometry_get_unique_segments_with_unspecified_interface_count
#define sgXd_geometry_get_unique_primitives_with_properties_conflict_count \
  sg2d_geometry_get_unique_segments_with_properties_conflict_count

#endif /* STAR_GEOMETRY2D_X_H__ */
