/* Copyright (C) 2019, 2020, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SG2D_SENC_2D_HELPER_H__
#define SG2D_SENC_2D_HELPER_H__

#include "sg2.h"
#include <star/senc2d.h>

#include <rsys/rsys.h>
#include <rsys/float2.h>

/* Get vertex indices for the iseg_th segment.
 * Suitable for use as get_indice callback in s2d_mesh_setup_indexed_vertices
 * calls. */
static FINLINE void
sg2d_s2d_geometry_get_indices
  (const unsigned iseg,
   unsigned indices[SG2D_GEOMETRY_DIMENSION],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  res_T r;
  ASSERT(indices && geometry);
  r = sg2d_geometry_get_unique_segment_vertices(geometry, iseg, indices);
  ASSERT(r == RES_OK); (void)r;
}

/* Get coordinates for the ivert_th vertex.
 * Suitable for use as s2d_vertex_data getter for S2D_POSITION s2d_attrib_usage
 * in s2d_mesh_setup_indexed_vertices calls. */
static FINLINE void
sg2d_s2d_geometry_get_position
  (const unsigned ivert,
   float coord[SG2D_GEOMETRY_DIMENSION],
   void* ctx)
{
  const struct sg2d_geometry* geometry = ctx;
  double tmp[2];
  res_T r;
  ASSERT(coord && geometry);
  r = sg2d_geometry_get_unique_vertex(geometry, ivert, tmp);
  ASSERT(r == RES_OK); (void)r;
  f2_set_d2(coord, tmp);
}

END_DECLS

#endif /* SG2D_SENC_2D_HELPER_H__ */
