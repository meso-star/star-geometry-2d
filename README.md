# Star-geometry-2d

The purpose of this library is to help create clean and decorated 2D
geometries. These geometries are suitable to be partitioned into
enclosures using the star-enclosures-2d library. It also provides
mechanisms to construct segment-related app data, detect
inconsistencies and dump the resulting geometry in various formats (OBJ,
VTK, C code chunks).

## How to build

Star-geometry-2d relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also
depends on the [RSys](https://gitlab.com/vaplv/rsys/) library.

First ensure that CMake and a C compiler that is compliant with C89 are
installed on your system. Then install the RCMake package as well as all
the aforementioned prerequisites. Finally generate the project from the
`cmake/CMakeLists.txt` file by appending to the `CMAKE_PREFIX_PATH`
variable the install directories of its dependencies.

## Release notes

### Version 0.1.1

- Fixed help headers failing to compile when included in C++ files.
- Fixed a typo in the `sgX2d.h` header apparently due to fingers getting
  tangled on the keyboard.

### Version 0.1

First version and implementation of the star-geometry-2d API.

- Creation of geometries in multiple steps, allowing advanced
  deduplication and application-data management
- Dump of geometries as OBJ or VTK files or as C code chunks

## License

Copyright © 2019, 2020, 2023 [|Méso|Star>](https://www.meso-star.com)
(<contact@meso-star.com>). It is free software released under the GPLv3+
license: GNU GPL version 3 or later. You are welcome to redistribute it under
certain conditions; refer to the COPYING files for details.
